<?php

namespace App\Entity;

use App\Enum\HealthStatus;

class Dinosaur
{
    private string $name;
    private string $genus;
    private int $length;
    private string $enclosure;
    private HealthStatus $health = HealthStatus::Healthy;

    public function __construct(string $name, string $genus = 'Unknown', int $length = 0, string $enclosure = 'Unknown')
    {
        $this->name = $name;
        $this->genus = $genus;
        $this->length = $length;
        $this->enclosure = $enclosure;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGenus(): string
    {
        return $this->genus;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getEnclosure(): string
    {
        return $this->enclosure;
    }

    public function getSizeDescription(): string
    {
        if (10 <= $this->length) {
            return 'Large';
        }
        if (4 < $this->length) {
            return 'Medium';
        }

        return 'Small';
    }

    /**
     * @return string
     */
    public function getHealth(): HealthStatus
    {
        return $this->health;
    }

    /**
     * @param string $health
     */
    public function setHealth(HealthStatus $health): void
    {
        //$this->health = HealthStatus::Healthy->value === $health ? HealthStatus::Healthy : HealthStatus::Sick;
        $this->health = $health;
    }

    public function isAcceptingVisitors(): bool
    {
        return HealthStatus::Sick !== $this->health;
    }


}
