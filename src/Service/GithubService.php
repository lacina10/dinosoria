<?php

namespace App\Service;

use App\Enum\HealthStatus;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GithubService
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger
    ) {
    }

    public function getDinoStatusFromLabel(array $labels): HealthStatus
    {
        $health = HealthStatus::Healthy;
        foreach ($labels as $label) {
            ['name' => $name] = $label;

            if (!str_starts_with($name, 'Status:')) {
                continue;
            }

            $status = trim(substr($name, strlen('Status:')));

            $health = HealthStatus::tryFrom($status);

            if (!$health) {
                throw new \RuntimeException(sprintf('%s is an unknown status label!', $status));
            }
        }

        return $health;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function getHealthReport(string $dinosaurName): HealthStatus
    {
        $health = HealthStatus::Healthy;
        // call Github API
        $response = $this->httpClient->request(
            Request::METHOD_GET,
            'https://api.github.com/repos/SymfonyCasts/dino-park/issues'
        );

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new \RuntimeException('Un problème de récupération des issues depuis Github à été détecter');
        }

        foreach ($response->toArray() as $issue) {
            if (str_contains($issue['title'], $dinosaurName)) {
                $health = $this->getDinoStatusFromLabel($issue['labels']); // ?? HealthStatus::Healthy;
                $this->logger->info('Resquest Dino issues', [
                    'dino' => $dinosaurName,
                    'responseData' => $response->getStatusCode(),
                ]);
            }
            /*if(str_contains($issue['title'], $dinosaurName)){
                return HealthStatus::Healthy->value === $issue['health'] ? HealthStatus::Healthy : HealthStatus::Sick;
            }*/
        }

        return $health;
    }
}
