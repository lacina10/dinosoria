<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Dinosaur;
use App\Enum\HealthStatus;
use PHPUnit\Framework\TestCase;

class DinosaurTest extends TestCase
{

    public function sizeDescriptionProvider(): \Generator
    {
        yield '10 meters large dino' => [10 , 'Large'];
        yield '5 meters medium dino' => [5 , 'Medium'];
        yield '3 meters small dino' => [3 , 'Small'];
    }

    public function testCanGetAndSetData(): void
    {
        $dinosaur = new Dinosaur('Big Eaty', 'Tyrannosaurus', 15, 'Paddock A');

        self::assertSame('Big Eaty', $dinosaur->getName());
        self::assertSame('Tyrannosaurus', $dinosaur->getGenus());
        self::assertSame(15, $dinosaur->getLength());
        self::assertSame('Paddock A', $dinosaur->getEnclosure());

        //self::assertGreaterThan($dinosaur->getLength(), 10, 'Dino is supposed to be bigger than 10 meters!');
    }

    /**
     * @dataProvider sizeDescriptionProvider
     */
    public function testDinoHasCorrectDescriptionFromLength(int $length, string $expectedSize): void
    {
        $dinosaur = new Dinosaur('Big Eaty', 'Tyrannosaurus', $length, 'Paddock A');
        self::assertSame($expectedSize, $dinosaur->getSizeDescription(), 'This is supposed to be a large Dinosaur');
    }

    public function healthStatusProvider(): \Generator
    {
        yield 'Sick dino is not accepting visitors' => [HealthStatus::Sick, false];
        yield 'Hungry dino is accepting visitors' => [HealthStatus::Hungry, true];
    }

    public function testDinoIsAcceptingVisitors(): void
    {
        $dinosaur = new Dinosaur('Dennis');
        self::assertTrue($dinosaur->isAcceptingVisitors());
    }

    /** @dataProvider HealthStatusProvider */
    public function testDinoIsNotAcceptingBasedOnHealthStatus(HealthStatus $healthStatus, bool $expectedVisitorsStatus): void
    {
        //$this->markTestIncomplete('I must write the health status in Dinosaur class before!');
        $dinosaur = new Dinosaur('Bumpy');
        $dinosaur->setHealth($healthStatus);
        self::assertSame($expectedVisitorsStatus, $dinosaur->isAcceptingVisitors());
        //self::assertFalse($dinosaur->isAcceptingVisitors());
    }

    /*public function testDinoUnder5MeterIsSmall(): void
    {
        $dinosaur = new Dinosaur('little Eaty', 'Tyrannosaurus', 3, 'Paddock A');
        self::assertSame('Small', $dinosaur->getSizeDescription(), 'This is supposed to be a
         small Dinosaur');
    }

    public function testDinoBetween5And9MeterIsMedium(): void
    {
        $dinosaur = new Dinosaur('Smith Eaty', 'Tyrannosaurus', 7, 'Paddock A');
        self::assertSame('Medium', $dinosaur->getSizeDescription(), 'This is supposed to be a
         medium Dinosaur');
    }
    public function testItWorks() :void
    {
        self::assertEquals('42', 42);
    }

    public function testItsWorksTheSame() :void
    {
        self::assertSame('42', 42);
    }*/
}