<?php

namespace App\Tests\Unit\Service;

use App\Enum\HealthStatus;
use App\Service\GithubService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GithubServiceTest extends TestCase
{
    private LoggerInterface $mockLogger;
    private HttpClientInterface $mockHttpClient;
    private ResponseInterface $mockResponse;

    protected function setUp(): void
    {
        $this->mockHttpClient = new MockHttpClient();
        $this->mockLogger = $this->createMock(LoggerInterface::class);
    }

    public function createGitHubService(array $responseData): GithubService
    {
        $this->mockResponse = new MockResponse(json_encode($responseData, JSON_THROW_ON_ERROR));
        $this->mockHttpClient->setResponseFactory($this->mockResponse);

        return new GithubService($this->mockHttpClient, $this->mockLogger);
    }

    public function dinosaurNameProvider(): \Generator
    {
        yield 'Sick dino' => [HealthStatus::Sick, 'Daisy'];
        yield 'Healthy dino' => [HealthStatus::Healthy, 'Maverick'];
    }

    /**
     * @dataProvider dinosaurNameProvider
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testGetHealthReportReturnsCorrectHealthStatusForDino($expectedStatus, $dinosaurName): void
    {
        /*
        //double test
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockClient = $this->createMock(HttpClientInterface::class);
        /*stubs
        * mockResponse et mockHttpClient sont maintenant officiellement appelés des stubs...
        * C'est une façon élégante de dire de faux objets où nous prenons éventuellement
        * le contrôle des valeurs qu'il renvoie.
        *
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse->method('getStatusCode')->willReturn(200);
        $mockResponse->method('toArray')->willReturn([
            ['title' => 'Daisy', 'labels' => [['name' => 'Status: Sick'],],],
            ['title' => 'Maverick', 'labels' => [['name' => 'Status: Healthy'],],],
            //['title' => 'Dennis', 'labels' => [['name' => 'Status: hungry'],],],
        ]);
        $mockClient->expects(self::atLeastOnce())->method('request')->with(
            'GET', 'https://api.github.com/repos/SymfonyCasts/dino-park/issues'
        )->willReturn($mockResponse);

        $service = new GithubService($mockClient, $mockLogger); */
        $service = $this->createGitHubService(
            [
                ['title' => 'Daisy', 'labels' => [['name' => 'Status: Sick'],],],
                ['title' => 'Maverick', 'labels' => [['name' => 'Status: Healthy'],],],
            ]
        );
        self::assertSame($expectedStatus, $service->getHealthReport($dinosaurName));

        self::assertSame(1, $this->mockHttpClient->getRequestsCount());
        self::assertSame('GET', $this->mockResponse->getRequestMethod());
        self::assertSame('https://api.github.com/repos/SymfonyCasts/dino-park/issues', $this->mockResponse->getRequestUrl());
        self::assertSame(200, $this->mockResponse->getStatusCode());
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testExceptionThrownWithUnknownLabel()
    {

     $dinosaurName = 'Maverick';

     /*$mockHttpClient = $this->createMock(HttpClientInterface::class);
     $mockLogger = $this->createMock(LoggerInterface::class);

     $mockResponse = $this->createMock(ResponseInterface::class);

     $mockResponse->method('getStatusCode')->willReturn(200);
     $mockResponse->method('toArray')->willReturn([
         ['title' => $dinosaurName, 'labels' => [['name' => 'Status: Drowsy'],],],
     ]);

     $mockHttpClient->method('request')->willReturn($mockResponse);

     --------------------- using built-in mock class of HttpClient Service -----------------
     $mockResponse = new MockResponse(
         json_encode([
             ['title' => $dinosaurName, 'labels' => [['name' => 'Status: Drowsy'],],],
         ], JSON_THROW_ON_ERROR)
     );
     $mockHttpClient = new MockHttpClient($mockResponse);

     $this->expectException(\RuntimeException::class);
     $this->expectExceptionMessage('Drowsy is an unknown status label!');

     $service = new GithubService($mockHttpClient, $this->createMock(LoggerInterface::class));
     */
     $service = $this->createGitHubService(
         [
             ['title' => $dinosaurName, 'labels' => [['name' => 'Status: Drowsy'],],],
         ]
     );

     $this->expectException(\RuntimeException::class);
     $this->expectExceptionMessage('Drowsy is an unknown status label!');

     $service->getHealthReport($dinosaurName);

     //self::assertSame(HealthStatus::Healthy, $service->getHealthReport($dinosaurName));
    }
}